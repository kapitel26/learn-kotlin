# Beschreibung der Übung

Erzeuge ein neues Ktor-Modul für einen Zweispieler-Modus.
Registriere dafür einen Handler für folgende URL: 

    http://localhost:8080/zweispieler/{spielid}/{spielerid}?wahl=papier

Die `spielid` ist frei wählbar und die `spielerid` ist entweder 1 oder 2.
Sobald sich für ein Spiel zwei Spieler gefunden haben wird das Ergebnis ausgegeben:

    <html>
      <body>
        <h1>Schnick Schnack Schnuck</h1>
        <p>Spieler1 hat gewählt: PAPIER</p>
        <p>Spieler2 hat gewählt: PAPIER</p>
        <p>Das Ergebnis ist: Unentschieden</p>
      </body>
    </html>

Solange noch ein Spieler nicht gewählt hat, ist das Ergebnis offen:

    <html>
      <body>
        <h1>Schnick Schnack Schnuck</h1>
        <p>Warte auf anderen Spieler.</p>
      </body>
    </html>

Erstelle eine Klasse `Spiel` In der `wahl1` und `wahl2` des jeweiligen Spielers abgelegt werden (beide optional).
Erstelle für das Ergebnis einen eigenen Enum mit folgenden Werten: 

    OFFEN, SPIELER1_GEWNNT, SPIELER2_GEWNNT, UNENTSCHIEDEN

Erstelle eine Funktion in der Klasse Spiel, 
welche das Ergebnis ermittelt: 

    fun ermittleErgebnis(): Ergebnis
 

## Manuelle Tests

Gehe auf die Seite: 

http://localhost:8080/zweispieler/a/1?wahl=papier 

und beachte das Ergebnis: Es soll `OFFEN` sein und `Warte auf anderen Spieler.` soll angezeigt werden.

Gehe auf die Seite: 

http://localhost:8080/zweispieler/a/2?wahl=stein 

und beachte das Ergebnis: `SPIELER1_GEWINNT`
    
## Zusatzaufgaben

* Nutze eine `ConcurrentHashMap` um die Zugriffe auf die Map thread-safe zu machen.
* Baue eine Einstiegsseite für das Spiel: `http://localhost:8080/zweispielerstart/{spielid}/{spielerid}`

    <html>
      <body>
        <h1>Schnick Schnack Schnuck</h1>
        <form action="/zweispieler/{spielid}/{spielerid}" method="get">
            <label>Deine Wahl:</label>
            <button name="wahl" type="submit" value="papier">Papier</button>
            <button name="wahl" type="submit" value="stein">Stein</button>
        </form>
      </body>
    </html>  

# Kenntnisse

* Klasse mit Konstruktor und Properties: `class Person(var name: String)`
* Methoden in einer Klasse: `class Person { fun sayHello() { println("Hello"} }`
* Map anlegen: `mutableMapOf() / mapOf()`
* Map-Zugriff: `m["hallo"]="welt"`

# Code-Beispiele
* `teil4-klassen-und-maps/src/main/kotlin/de/e2/adder`
* `teil4-klassen-und-maps/src/test/kotlin/de/e2/misc`