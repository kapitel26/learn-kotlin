### Chat (Multiplatform-Project)
* [Kotlin Chat Client](src/jvmMain/kotlin/de/e2/ktor/multiplatform/ChatClientKotlin.kt)
* [Common Chat Client API](src/commonMain/kotlin/de/e2/ktor/multiplatform/ChatClient.kt)
* [HTML Chat Client](src/jvmMain/resources/index.html) 
            ([JavaScript](src/jsMain/kotlin/de/e2/ktor/multiplatform/ChatClientJs.kt))
* [Logging](src/commonMain/kotlin/de/e2/ktor/multiplatform/Logging.kt)
            ([Kotlin](src/jvmMain/kotlin/de/e2/ktor/multiplatform/LoggingJvm.kt),
            [JavaScript](src/jsMain/kotlin/de/e2/ktor/multiplatform/LoggingJs.kt)) 
