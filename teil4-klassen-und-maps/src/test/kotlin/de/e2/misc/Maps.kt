package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class Maps {

    @Test
    fun `Pairs`() {

        val p1 =
                Pair(2, 4)

        assertEquals(2, p1.first)
        assertEquals(4, p1.second)

        val p2 =
                47 to 11 // Infix Notation

        assertEquals(47, p2.first)
        assertEquals(11, p2.second)
    }

    @Test
    fun `Maps`() {

        val map = mapOf(
            "König" to 4,
            "Dame" to 3,
            "Bube" to 2
        )

        assertEquals(2, map["Bube"])
        assertEquals(3, map["Dame"])
        assertEquals(4, map["König"])
        assertNull( map["As"])
        assertEquals( 0, map.getOrDefault("Pferd",0))
    }

    @Test
    fun `Veränderbare (mutable) Maps gibt es natürlich auch`() {

        val map = mutableMapOf("König" to 4)

        assertNull(map["Dame"])

        map["Dame"] = 3

        assertEquals(3, map["Dame"])
        assertEquals(4, map["König"])
    }

    @Test
    fun `Und die guten Alten von Java kann man auch nehmen, wenn man möchte`() {

        val map1 = java.util.HashMap<String, Int>()
        map1["König"] = 4
        map1["Dame"] = 3

        assertEquals(3, map1["Dame"])
        assertEquals(4, map1["König"])
    }
}