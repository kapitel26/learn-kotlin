package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * ## Nützliche Extension Functions
 *
 */
class StandardExtensionFunktionen {

    /**
     * Listen und Strings haben viele nützliche Extensions
     */
    @Test
    fun `Nuetzliche Extensions`() {

        val numberList = listOf(1, 2)
        val sum = numberList.sum()
        assertEquals(3, sum)

        val first = numberList.first()
        assertEquals(1, first)

        numberList.forEachIndexed { index, i ->
            assertEquals(index, i - 1 )
        }

        assertEquals("Abc", "abc".capitalize())
        assertEquals(null, "abc".toIntOrNull())
    }
}


