package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.io.File
import java.math.RoundingMode
import java.util.*

/**
 * ## Scope-Funktionen
 * * die folgenden Funktionen erlauben idiomatischen Kotlin-Code
 * * sie vermeiden häufig Hilfsvariablen und komplexe Ausdrücke
 *
 * Für die Auswahl siehe:
 * ['Mastering Kotlin standard functions: run, with, let, also and apply'](https://medium.com/@elye.project/mastering-kotlin-standard-functions-run-with-let-also-and-apply-9cd334b0ef84)
 */
class ScopeFunktionen {

    /**
     * Um mehrfach auf dasselbe Objekt  (hier 3)  zuzugreifen,
     * kann man eine Hilfsvariable einführen.
     */
    @Test
    fun `Hilfsvariable, um dasselbe Objekt mehrfach zu nutzen`() {
        val seitenlaenge = 3
        assertEquals(
                27,
                seitenlaenge * seitenlaenge * seitenlaenge
        )
    }

    /**
     * Anstelle von Hilfsvariablen können auch Parameter eines Lambdas
     * genutzt werden, um mehrfach auf dasselbe Objet zugreifen zu können.
     */
    @Test
    fun `let mit Lambda, um dasselbe Objekt (hier 3) mehrfach zu nutzen`() {
        assertEquals(
                27,
                3.let { it * it * it }
        )
    }

    /**
     * Doch warum sollte man das tun? Hier ein Beispiel:
     *
     * Kurze Hilfsfunktionen werden in Kotlin gerne als Single-Statement
     * (mit `=`) deklariert,
     * weil dies kompakter ist als eine volle
     * Deklaration mit geschweiften Klammern und `return`.
     * `let` ermöglicht es eine Codeabschnitt mit Hilfsvariable in ein
     *  Single-Statement umzuwandeln.
     */
    @Test
    fun `Single-Statement-Deklaration mit let`() {

        fun e2square_withBrackets(e: String) : Double {
            val d = e.toDouble()
            return d * d
        }

        fun e2square_singleStatement(e: String) = e.toDouble().let { it * it }

        assertEquals(
                e2square_withBrackets("42"),
                e2square_singleStatement("42")
        )
    }

    class Adresse(
            val adressat: String,
            val strasse: String,
            val hausnummer: String,
            val plz: String,
            val ort: String
    )

    /**
     * Nutzt man mehrere Attribute desselben Objekts,
     * bläht die wiederholte Referenz, hier `adresse.`,
     * den Code auf und erschwert die Lesbarkeit.
     */
    @Test
    fun `wiederholte Referenzen`() {
        val adresse = Adresse("Rheingoldhalle","Rheinstraße", "66", "55116", " Mainz")
        println(adresse.adressat)
        println("${adresse.strasse} ${adresse.hausnummer}")
        println("${adresse.plz} ${adresse.ort}")
    }

    /**
     * `with` ist eine  Scope-Funktion,
     * die einen Block mit dem gegebenen Objekt
     * als Receiver (aka `this`) ausführt.
     *
     * Hiermit werden die Referenzen auf die Hilfsvariable reduziert.
     */
    @Test
    fun `scoping mit with`() {
        val adresse = Adresse("Rheingoldhalle","Rheinstraße", "66", "55116", " Mainz")
        with(adresse) {
            println(adressat)
            println("$strasse $hausnummer")
            println("$plz $ort")
        }
    }

    /**
     * `run` ist eine  Scope-Funktion,
     * die einen Block mit dem gegebenen Objekt
     * als Receiver (aka `this`) ausführt.
     *
     * Auch mit `run` kann man Hilfsvariablen oft eliminieren.
     */
    @Test
    fun `scoping ohne Hilfsvariable mit run`() {
        Adresse("Rheingoldhalle","Rheinstraße", "66", "55116", " Mainz").run {
            println(adressat)
            println("$strasse $hausnummer")
            println("$plz $ort")
        }
    }

    class Quader(val hoehe: Int, val breite: Int, val tiefe: Int)

    /**
     * Der Rückgabewert des `run`-Blocks kann weiter genutzt werden.
     * dies ermöglicht Transformationen.
     */
    @Test
    fun `Transformation mit run`() {
        assertEquals(
                60,
                Quader(3,4,5).run { hoehe * breite * tiefe }
        )
    }

    /**
     * Die Java-Klasse `Properties` kann über den Konstruktor
     * nicht mit Daten befüllt werden.
     * Mit `run` kann man sie trotzdem in einem Schritt befüllen .
     */
    @Test
    fun `run`() {
        Properties()
                .run {
                    put("hallo", "hello")
                    put("welt", "world")
                }
    }


    /**
     * `also` ist eine Scoping-Funktion,
     * die das Receiver-Objekt selber wieder zurückgibt.
     * Dies ermöglicht es beispielsweise Seiteneffekte
     * "dazwischen zu schieben".
     */
    @Test
    fun `Seiteneffekte mit also`() {
        val cleansedValue = "  00047.107  "
                .let { it.trim() }.also { println(it) }
                .let { it.toBigDecimal() }.also { println(it) }
                .let { it.setScale(2, RoundingMode.HALF_UP) }.also { println(it) }
                .let { it.toString() }

        assertEquals("47.11", cleansedValue)
    }

    /**
     * 'use' ist der Ersatz für `try-with-resources` aus Java
     * `use` ist eine Extension-Funktion für ein `Closable`
     */
    @Test
    fun `Use schließt Resourcen nach der Benutzung`() {

        File("build/numbers.txt").writer().use { writer ->
            (1..5).forEach {
                writer.write(it)
            }
        }
    }


}


