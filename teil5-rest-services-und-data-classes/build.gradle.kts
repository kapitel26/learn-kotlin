val ktorVersion: String by rootProject
val junitVersion: String by rootProject
val log4jVersion: String by rootProject
val jsonpathVersion: String by rootProject


plugins {
    kotlin("jvm")
}


dependencies {
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-html-builder:$ktorVersion")
    implementation("io.ktor:ktor-jackson:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")
    implementation("io.ktor:ktor-client-json:$ktorVersion")
    implementation("io.ktor:ktor-client-jackson:$ktorVersion")
    implementation("com.jayway.jsonpath:json-path:$jsonpathVersion")
    implementation("com.jayway.jsonpath:json-path:$jsonpathVersion")

    testImplementation("org.junit.jupiter:junit-jupiter:$junitVersion")

    runtimeOnly("org.apache.logging.log4j:log4j-slf4j-impl:$log4jVersion")
}

