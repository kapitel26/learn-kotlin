### Ausblick

* [Vererbung und Interfaces (bst)](src/test/kotlin/de/e2/Klassen.kt)
* [Import Aliases (bst)](src/test/kotlin/de/e2/Aliases.kt)
* [Objects und Companions](src/test/kotlin/de/e2/ObjectsUndCompanions.kt)
* [Annotations, sealed classsed, Jackson (bst)](src/test/kotlin/de/e2/JacksonAndAnnotations.kt)
* [Generics, inline reified (bst)](src/test/kotlin/de/e2/Generics.kt)
      
   
* Exoten für Library-Entwickler (rp)
    * [Lokale Extensions](src/main/kotlin/de/e2/kson/KSon1.kt)
    * [Infix Funktionen](src/main/kotlin/de/e2/kson/KSon2.kt)
    * [Operatoren](src/main/kotlin/de/e2/kson/KSon3.kt)    
    * [Delegated Properties](src/main/kotlin/de/e2/delegation/DelegatedIntro.kt)
        ([B](src/main/kotlin/de/e2/delegation/DelegationMap.kt), [C](../loesungen/build.gradle.kts))


* Multiplatform (rp)
    * [Kotlin Chat Client](../mpp-chat/src/jvmMain/kotlin/de/e2/ktor/multiplatform/ChatClientKotlin.kt)
    * [Common Chat Client API](../mpp-chat/src/commonMain/kotlin/de/e2/ktor/multiplatform/ChatClient.kt)
    * [HTML Chat Client](../mpp-chat/src/jvmMain/resources/index.html) 
                ([JavaScript](../mpp-chat/src/jsMain/kotlin/de/e2/ktor/multiplatform/ChatClientJs.kt))
    * [Logging](../mpp-chat/src/commonMain/kotlin/de/e2/ktor/multiplatform/Logging.kt)
                ([Kotlin](../mpp-chat/src/jvmMain/kotlin/de/e2/ktor/multiplatform/LoggingJvm.kt),
                [JavaScript](../mpp-chat/src/jsMain/kotlin/de/e2/ktor/multiplatform/LoggingJs.kt))



