package de.e2.schnick.loesung6

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.e2.schnick.p6.ApiRequest
import de.e2.schnick.p6.ApiResponse
import de.e2.schnick.p6.Ergebnis
import de.e2.schnick.p6.SchnickSchnackSchnuckService
import de.e2.schnick.p6.Wahl
import de.e2.schnick.p6.ermittleErgebnis
import de.e2.schnick.p6.restModul
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.nulls.beNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.ktor.application.Application
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class RestApiTest : StringSpec({
    "ermittleErgebnis" {
        assertSoftly {
            forAll(
                row(Wahl.STEIN, Wahl.PAPIER, Ergebnis.COMPUTER_GEWINNT),
                row(Wahl.STEIN, Wahl.SCHERE, Ergebnis.SPIELER_GEWINNT),
                row(Wahl.STEIN, Wahl.STEIN, Ergebnis.UNENTSCHIEDEN),

                row(Wahl.PAPIER, Wahl.SCHERE, Ergebnis.COMPUTER_GEWINNT),
                row(Wahl.PAPIER, Wahl.STEIN, Ergebnis.SPIELER_GEWINNT),
                row(Wahl.PAPIER, Wahl.PAPIER, Ergebnis.UNENTSCHIEDEN),

                row(Wahl.SCHERE, Wahl.STEIN, Ergebnis.COMPUTER_GEWINNT),
                row(Wahl.SCHERE, Wahl.PAPIER, Ergebnis.SPIELER_GEWINNT),
                row(Wahl.SCHERE, Wahl.SCHERE, Ergebnis.UNENTSCHIEDEN)
            ) { spielerwahl, computerWahl, ergebnis ->
                ermittleErgebnis(spielerwahl, computerWahl) shouldBe ergebnis
            }
        }
    }
    "integration test" {
        withTestApplication(Application::restModul) {
            val call = handleRequest(HttpMethod.Post, "/api") {
                setBody(
                    """ 
                    {
                      "wahl": "STEIN"
                    }
                    """
                )
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            }
            with(call) {
                response.status() shouldBe HttpStatusCode.OK

                val contentType = response.headers[HttpHeaders.ContentType]
                contentType.shouldNotBeNull()
                ContentType.parse(contentType).withoutParameters() shouldBe ContentType.Application.Json

                val content = response.content
                content.shouldNotBeNull()
                //parsing is enough
                jacksonObjectMapper().readValue<ApiResponse>(content)
            }
        }
    }

    "mock random" {
        val mockRandom = mockk<Random>()
        every { mockRandom.nextInt(any()) } returns 0
        val sss = SchnickSchnackSchnuckService(mockRandom)
        val request = ApiRequest(Wahl.STEIN)
        val response = sss.spielGegenComputer(request)

        response.computerWahl shouldBe Wahl.PAPIER
        response.ergebnis shouldBe Ergebnis.COMPUTER_GEWINNT
    }
})