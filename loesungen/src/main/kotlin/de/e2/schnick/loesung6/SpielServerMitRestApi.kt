package de.e2.schnick.p6

import de.e2.schnick.p6.Ergebnis.COMPUTER_GEWINNT
import de.e2.schnick.p6.Ergebnis.SPIELER_GEWINNT
import de.e2.schnick.p6.Ergebnis.UNENTSCHIEDEN
import de.e2.schnick.p6.Wahl.PAPIER
import de.e2.schnick.p6.Wahl.SCHERE
import de.e2.schnick.p6.Wahl.STEIN
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlin.random.Random

enum class Wahl {
    PAPIER, STEIN, SCHERE
}

enum class Ergebnis {
    SPIELER_GEWINNT, COMPUTER_GEWINNT, UNENTSCHIEDEN

}

fun ermittleErgebnis(spielerWahl: Wahl, computerWahl: Wahl): Ergebnis = when {
    spielerWahl == computerWahl -> UNENTSCHIEDEN
    spielerWahl == PAPIER && computerWahl == STEIN -> SPIELER_GEWINNT
    spielerWahl == STEIN && computerWahl == SCHERE -> SPIELER_GEWINNT
    spielerWahl == SCHERE && computerWahl == PAPIER -> SPIELER_GEWINNT
    else -> COMPUTER_GEWINNT
}

data class ApiRequest(val wahl: Wahl)
data class ApiResponse(val spielerWahl: Wahl, val computerWahl: Wahl, val ergebnis: Ergebnis)

class SchnickSchnackSchnuckService(val random: Random) {
    fun spielGegenComputer(apiRequest: ApiRequest): ApiResponse {
        val computerWahl = Wahl.values().random(random)
        val ergebnis = ermittleErgebnis(apiRequest.wahl, computerWahl)
        return ApiResponse(apiRequest.wahl, computerWahl, ergebnis)
    }
}

fun Application.restModul() {
    install(ContentNegotiation) {
        jackson()
    }

    val service = SchnickSchnackSchnuckService(Random)

    routing {
        post("/api") {
            val apiRequest = call.receive<ApiRequest>()
            call.respond(service.spielGegenComputer(apiRequest))
        }
    }
}


fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::restModul)

    server.start(wait = true)
}
