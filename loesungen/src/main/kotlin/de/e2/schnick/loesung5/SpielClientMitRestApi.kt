package de.e2.schnick.loesung5

import de.e2.schnick.p5.ApiRequest
import de.e2.schnick.p5.ApiResponse
import de.e2.schnick.p5.Wahl
import io.ktor.client.HttpClient
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.ContentType
import io.ktor.http.contentType
import kotlinx.coroutines.runBlocking

fun main(): Unit = runBlocking {
    val client = HttpClient() {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }
    }


    val response = client.post<ApiResponse> {
        url("http://localhost:8080/api")
        contentType(ContentType.Application.Json)
        body = ApiRequest(Wahl.STEIN)
    }

    println("Das Ergebnis ist: $response")
}