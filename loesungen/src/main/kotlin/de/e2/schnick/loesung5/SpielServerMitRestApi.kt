package de.e2.schnick.p5

import de.e2.schnick.p5.Ergebnis.COMPUTER_GEWINNT
import de.e2.schnick.p5.Ergebnis.SPIELER_GEWINNT
import de.e2.schnick.p5.Ergebnis.UNENTSCHIEDEN
import de.e2.schnick.p5.Wahl.PAPIER
import de.e2.schnick.p5.Wahl.SCHERE
import de.e2.schnick.p5.Wahl.STEIN
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

enum class Wahl {
    PAPIER, STEIN, SCHERE
}

enum class Ergebnis {
    SPIELER_GEWINNT, COMPUTER_GEWINNT, UNENTSCHIEDEN

}

fun ermittleErgebnis(spielerWahl: Wahl, computerWahl: Wahl): Ergebnis = when {
    spielerWahl == computerWahl -> UNENTSCHIEDEN
    spielerWahl == PAPIER && computerWahl == STEIN -> SPIELER_GEWINNT
    spielerWahl == STEIN && computerWahl == SCHERE -> SPIELER_GEWINNT
    spielerWahl == SCHERE && computerWahl == PAPIER -> SPIELER_GEWINNT
    else -> COMPUTER_GEWINNT
}

data class ApiRequest(val wahl: Wahl)
data class ApiResponse(val spielerWahl: Wahl, val computerWahl: Wahl, val ergebnis: Ergebnis)

fun Application.restModul() {
    install(ContentNegotiation) {
        jackson()
    }

    routing {
        post("/api") {
            val apiRequest = call.receive<ApiRequest>()

            val computerWahl = Wahl.values().random()
            val ergebnis = ermittleErgebnis(apiRequest.wahl, computerWahl)

            call.respond(ApiResponse(apiRequest.wahl, computerWahl, ergebnis))
        }
    }
}


fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::restModul)

    server.start(wait = true)
}
