package de.e2.schnick.loesung1

import io.ktor.application.call
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/spiel") {
                call.respondTextWriter {
                    repeat(5) { index ->
                        write("Schnick Schnack Schnuck $index")
                    }
                }
            }
        }
    }

    server.start(wait = true)
}
