# Funktionen und Datentypen

* [JUnit Tests (bst)](src/test/kotlin/de/e2/misc/JUnitTests.kt)              
* [Basisdatentypen (bst)](src/test/kotlin/de/e2/misc/Basisdatentypen.kt)              
* [Null Safety (bst)](src/test/kotlin/de/e2/misc/NullSafety.kt) 
             ([A](src/main/kotlin/de/e2/helloworld/HelloWorld_3a_Nullable.kt) 
             [B](src/main/kotlin/de/e2/helloworld/HelloWorld_3b_If_Expression.kt),
             [C](src/main/kotlin/de/e2/helloworld/HelloWorld_3c_Fragezeichen.kt),
             [D](src/main/kotlin/de/e2/helloworld/HelloWorld_3d_Elvis.kt),
             [E](src/main/kotlin/de/e2/helloworld/HelloWorld_3e_Elvis_Mit_Throw.kt))
* [Listen (bst)](src/test/kotlin/de/e2/misc/Listen.kt)           
* [Eigene Funktion (bst)](src/main/kotlin/de/e2/helloworld/HelloWorld_4a_Eigene_Funktion_Mit_Return.kt) 
            ([B](src/main/kotlin/de/e2/helloworld/HelloWorld_4b_Funktions_Statement.kt))
* [`if` in Ausdrücken (bst)](src/test/kotlin/de/e2/misc/IfInAusdruecken.kt)                        
* [Enums und Whens (bst)](src/test/kotlin/de/e2/misc/EnumsUndWhen.kt)                        
* [Uebung 2](../uebungen/Uebung2.md)