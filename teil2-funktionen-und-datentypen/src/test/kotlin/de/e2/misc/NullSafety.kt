package de.e2.misc

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.ClassCastException
import java.lang.IllegalArgumentException

class NullSafety {

    @Test
    fun `Anders als in Java, erlauben normale Deklarationen, keine null-Zuweisung`() {

        var eineVariable: String = "Hallo Welt!"

        // eineVariable = null      // -> Compile-Fehler

        assertNotNull(eineVariable) // eineVariable ist null-safe
    }

    @Test
    fun `optionale Wert werden mit einem ? gekennzeichnet`() {

        var eineVariable: String? = "Hallo Welt!"

        assertEquals("Hallo Welt!", eineVariable)

        eineVariable = null

        assertNull(eineVariable) // eineVariable ist null-safe

        // ACHTUNG: `String`-Methoden sind auf einer `String?`-Variable nicht verfügbar!

        // eineVariable.length  // -> Compile-Fehler
    }

    @Test
    fun `der as-Operator erlaubt einen casting auf den null-freien Typ`() {

        var eineVariable: String? = "Hallo Welt!"

        assertEquals((eineVariable as String).length, 11)

        // ACHTUNG: Das casting kann abhängig vom Inhalt der Variablen zur Laufzeit-Exception führen.

        eineVariable = null

        assertThrows<NullPointerException> { (eineVariable as String).length }

        // UM Laufzeitfehler zu vermeiden, müsste man den Cast mit einer
        // Abfrage kombinieren.
        // Sicher aber umständlich:

        if(eineVariable != null)
            assertEquals((eineVariable as String).length,11)
        else
            assertNull(eineVariable)
    }

    /**
     * Der `as`-Operator muss zum Glück nur selten direkt aufgerufen werden.
     * Denn der Compiler führ *smart casts* durch, wenn er sicher weiss,
     * dass in einer gegebenen Variable kein null-Wert enthalten ist.
     */
    @Test
    fun `der compiler führt smart casts durch, wenn aus dem context klar ist, was die Variable enthält`() {

        var eineVariable: String? = "Hallo Welt!"

        if(eineVariable != null)
            assertEquals(11, eineVariable.length)
        else
            assertNull(eineVariable)

        // TIPP: Setze den Curser auf `eineVariable`,
        // drücke dann Ctrl+Shift+p, Intellij
        // zeigt dann, welchen Datentyp der Compiler für diese Stelle
        // ermittelt hat.
    }

    /**
     * Sehr nützlich ist der safe-call-Operator `?.` führ eine Operation,
     * z. B. `a?.b`, nur
     * dann durch, wenn Sie *safe* (d.h. `a` ungleich `null`) ist,
     * andernfalls liefert er `null`.
     *
     * Verkettungen sind natürlich möglich, z. B.
     *
     * `val hausnummer: String? = kunde.kontaktdaten?.anschrift?.hausnummer`
     */
    @Test
    fun `der Safe-Call-Operator`() {
        var eineVariable: String? = "Hallo Welt!"

        assertEquals(11, eineVariable?.length)

        eineVariable = null

        assertNull(eineVariable?.length)

        // Der safe-call-Operator liefert als Ergebnis selber einen `?`-Datentyp.

        val result: Int? = eineVariable?.length
    }

    /**
     * Wenn man einen `null`-Wert durch einen Default-Wert ersetzen will,
     * hilft der Elvis-Operator ?:
     */
    @Test
    fun `der Elvis-Operator`() {
        var eineVariable: String? = "Hallo Welt!"

        assertEquals("Hallo Welt!", eineVariable ?: "<unbekannt>")

        eineVariable = null

        assertEquals("<unbekannt>", eineVariable ?: "<unbekannt>")

        // Statt einer Rückgabe, kann man natürlich auch mit einer Exception abbrechen:

        assertThrows<IllegalStateException> { eineVariable ?: throw IllegalStateException("Soll nicht sein!") }
    }


    @Test
    fun `ein paar Beispiele in kurzform`() {
        var eineVariable: String?

        if( Math.random() > 0.5 )
            eineVariable = "Hallo Welt!"
        else
            eineVariable = null

        println("eineVariable=$eineVariable")

        if(eineVariable != null) {
            val perSmartCast = eineVariable.length
            println("Smart Cast hat ergeben `eineVariable` ist ein String der Länge $perSmartCast.")
        } else {
            println("Smart Cast hat ergeben `eineVariable` ist null.")
        }

        val perSafeCall = eineVariable?.length
        println("Längenermittlung per `eineVariable?.length`: Der Länge ist $perSafeCall")

        val perElvis = eineVariable?.length ?: "nicht ermittelbar"
        println("Längenermittlung mit Elvis: Die Länge ist $perElvis")
    }


}


