# Testen und Mocking

* [kotest (bst)](src/test/kotlin/de/e2/testen/KoTest.kt)
* [Ktor Test Support (bst)](src/test/kotlin/de/e2/testen/AdderKtorTest.kt)
* [Mocking mit Mockk (bst)](src/test/kotlin/de/e2/testen/Mocking.kt)